<?php 
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');


    $sheep = new Animal ("shaun", 2, 'false');
    
    echo $sheep ->name. "<br>";
    echo $sheep ->legs. "<br>";
    echo $sheep ->cold_blooded . "<br>";
   
    echo "<br>";
    echo "<br>";

    $kodok = new Frog ('buduk', '4', 'hop hop');

    echo $kodok ->name . "<br>";
    echo $kodok ->feet. "<br>";
    echo $kodok -> jump(); 
    
    echo "<br>";
    echo "<br>";

    $sungokong = new Ape ('Kera Sakti', 2, 'Auooo');

    echo $sungokong ->name . "<br>";
    echo $sungokong ->feet. "<br>";
    echo $sungokong -> yell(); 
   ?>